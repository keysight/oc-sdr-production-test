# README #

### What is this repository for? ###

* This repository contains the source code of the RF test automation solution for OpenCellular SDR.
* The solution is in the form of plugins for Keysight Test Automation Platform (TAP). More information about TAP can be found at Keysight's website.

### Contribution guidelines ###

* Contributors to this project will follow the Git workflow (http://www.asmeurer.com/git-workflow/).

### Who do I talk to? ###

* Repository Owner: Bradley Hutchinson (bradley.hutchinson@keysight.com)
* For more information about Keysight's contribution to OpenCellular, contact Mark Chun (mark.chun@keysight.com)