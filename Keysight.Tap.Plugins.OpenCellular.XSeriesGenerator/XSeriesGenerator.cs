﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Serialization;

namespace Keysight.Tap.Plugins.OpenCellular.XSeriesGenerator
{
    /// <summary>
    /// Contains all basic Arbitrary Waveform control for Keysight X Series Signal Generators
    /// </summary>
    [ShortName("SigGen")]
    [Display("X-Series SigGen", Description: "Keysight X Series Vector Signal Generator (EXG/MXG)", Group: "OpenCellular")]
    public class XSeriesGenerator : ScpiInstrument
    {
        #region Settings
        [XmlIgnore]
        public SourceTriggerEnum Trigger { get; set; }
        [XmlIgnore]
        public TriggerContinuousEnum TriggerContinuous { get; set; }
        [XmlIgnore]
        public TriggerSingleEnum TriggerSingle { get; set; }
        [XmlIgnore]
        public TriggerGatedEnum TriggerGated { get; set; }
        [XmlIgnore]
        public TriggerSegAdvEnum TriggerSegAdv { get; set; }
        [XmlIgnore]
        public TriggerSourceEnum TriggerSource { get; set; }
        [XmlIgnore]
        public TriggerExtSourceEnum TriggerExtSource { get; set; }
        [XmlIgnore]
        public bool ExtDelay { get; set; }
        [XmlIgnore]
        public double ExtDelayTime { get; set; }
        [XmlIgnore]
        public double RuntimeScaling { get; set; }
        [XmlIgnore]
        public bool Modulation { get; set; }
        [XmlIgnore]
        public bool Rf { get; set; }
        [XmlIgnore]
        public double Frequency { get; set; }
        [XmlIgnore]
        public double Amplitude { get; set; }
        #endregion

        public void Apply()
        {
            ScpiCommand(Scpi.Format(":RAD:ARB:TRIG:TYPE {0}", Trigger));
            ScpiCommand(Scpi.Format(":RAD:ARB:TRIG:TYPE:CONT {0}", TriggerContinuous));
            ScpiCommand(Scpi.Format(":RAD:ARB:RETR {0}", TriggerSingle));
            ScpiCommand(Scpi.Format(":RAD:ARB:TRIG:TYPE:GATE {0}", TriggerGated));
            ScpiCommand(Scpi.Format(":RAD:ARB:TRIG:TYPE:SADV {0}", TriggerSegAdv));
            ScpiCommand(Scpi.Format(":RAD:ARB:TRIG {0}", TriggerSource));
            ScpiCommand(Scpi.Format(":ROUT:LINE:PTR1:BNC:SOUR {0}", TriggerExtSource));
            ScpiCommand(Scpi.Format(":RAD:ARB:TRIG:EXT:DEL:STAT {0}", ExtDelay));
            //ScpiCommand(Scpi.Format(":RAD:ARB:TRIG:EXT:DEL {0}", ExtDelayTime)); TODO: Value of 0 breaks, introduce parameter to Test Step?
            ScpiCommand(Scpi.Format(":RAD:ARB:RSC {0}", RuntimeScaling));
            ScpiCommand(Scpi.Format(":OUTP:MOD {0}", Modulation));
            ScpiCommand(Scpi.Format(":OUTP {0}", Rf));
            ScpiCommand(Scpi.Format(":FREQ {0}", Frequency));
            ScpiCommand(Scpi.Format(":POW {0}", Amplitude));
        }

        public XSeriesGenerator()
        {
            IoTimeout = 5000;
        }

        /// <summary>
        /// Smart function that selects waveform if it exists in VSG memory, loads and selects waveform if file name not found.
        /// </summary>
        /// <param name="fileName">Name of the file on the Source.</param>
        /// <param name="filePath">The local file path.</param>
        public void SetupWaveform(string fileName, string filePath = "")
        {
            if (string.IsNullOrEmpty(fileName))
                Log.Warning("Setup Waveform failed, File Name field is empty");
            else if (StoredVolatileWaveforms().Contains("\"" + fileName.ToUpper()))
                SelectWaveform(fileName);
            else if (StoredNonVolatileWaveforms().Contains("\"" + fileName.ToUpper() + ".WFM@SNVWFM"))
            {
                CopyWfmFromNonVolatile(fileName);
                SelectWaveform(fileName);
            }
            else if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                LoadWaveformToVolatile(fileName, filePath);
                SelectWaveform(fileName);
            }
            else
                Log.Warning("Setup Waveform failed. No waveform loaded!");
        }

        /// <summary>
        /// Uploads waveform file to Source NonVolatile memory.
        /// </summary>
        /// <param name="destinationFileName">Name of the file in Source memory.</param>
        /// <param name="localFilePathName">Name of the local file path.</param>
        public void LoadWaveformToNonVolatile(string destinationFileName, string localFilePathName)
        {
            string address = VisaAddress.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[1];
            string ftpPath;
            if (localFilePathName.EndsWith(".wfm"))
                ftpPath = "ftp://" + address + "/securewave/";
            else
                ftpPath = "ftp://" + address + "/waveform/";

            using (WebClient client = new WebClient())
                client.UploadFile(ftpPath + destinationFileName, localFilePathName);
        }

        /// <summary>
        /// Uploads waveform file to Source Volatile memory
        /// </summary>
        /// <param name="destinationFileName">Name of the file in Source memory.</param>
        /// <param name="localFilePathName">Name of the local file path.</param>
        public void LoadWaveformToVolatile(string destinationFileName, string localFilePathName)
        {
            string ftpPath;
            string address = VisaAddress.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries)[1];

            if(localFilePathName.EndsWith(".wfm"))
                ftpPath = "ftp://" + address + "/BBG1/securewave/";
            else
                ftpPath = "ftp://" + address + "/BBG1/waveform/";

            using (WebClient client = new WebClient())
                client.UploadFile(ftpPath + destinationFileName, localFilePathName);
        }

        /// <summary>
        /// Selects waveform file or sequence and enables the Dual Arb player. Do not include "WFM1:" header
        /// <para> File must be in volatile memory (WFM1). If it is in non-volatile
        /// memory, use CopyWfmFromNonVolatile() prior to calling this method. </para>
        /// </summary>
        /// <param name="name">The waveform name.</param>
        public void SelectWaveform(string name)
        {
            ScpiCommand(":RAD:ARB:WAV \"WFM1:{0}\"", name);
            ScpiCommand(":RAD:ARB ON");
        }

        /// <summary>
        /// Copies waveform file from non-volatile memory (SNVWFM) to volatile memory (WFM1).
        /// <para>If no destination file name is specified, file name will be unchanged</para>
        /// </summary>
        /// <param name="sourceName">Name of the source file in Non-Volatile memory.</param>
        /// <param name="destinationName">Name of the destination file in Volatile memory.</param>
        private void CopyWfmFromNonVolatile(string sourceName, string destinationName = null)
        {
            if (destinationName == null)
                ScpiCommand(":MEM:COPY \"SNVWFM:{0}\",\"WFM1:{0}\"", sourceName);
            else
                ScpiCommand(":MEM:COPY \"SNVWFM:{0}\",\"WFM1:{1}\"", sourceName, destinationName);
        }

        private List<string> StoredVolatileWaveforms()
        {
            return ScpiQuery(":MMEM:CAT? \"WFM1\"").Split(',').ToList();
        }

        private List<string> StoredNonVolatileWaveforms()
        {
            return ScpiQuery(":MEM:CAT?").Split(',').ToList();
        }
    }
}
