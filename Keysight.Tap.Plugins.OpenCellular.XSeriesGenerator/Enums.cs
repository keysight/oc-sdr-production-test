﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
namespace Keysight.Tap.Plugins.OpenCellular.XSeriesGenerator
{
    /// <summary>
    /// The available trigger types
    /// </summary>
    public enum SourceTriggerEnum
    {
        /// <summary>
        /// Continuous triggering repeats the waveform
        /// </summary>
        [Scpi("CONT")]
        Continuous,
        /// <summary>
        /// Single triggering plays the waveform once per trigger
        /// </summary>
        [Scpi("SING")]
        Single,
        /// <summary>
        /// Gated triggering enables the trigger to turn the waveform on and off
        /// </summary>
        [Scpi("GATE")]
        Gated,
        /// <summary>
        /// Segment Advance triggering moves between multiple waveform segments via triggering
        /// </summary>
        [Scpi("SADV")]
        Segment_Advance
    }

    /// <summary>
    /// Options for the Continuous trigger type
    /// </summary>
    public enum TriggerContinuousEnum
    {
        /// <summary>
        /// Play continuously without waiting for a trigger.
        /// </summary>
        [Scpi("FREE")]
        Free_Run,
        /// <summary>
        /// Play continuously at first trigger and ignore subsequent triggers.
        /// </summary>
        [Scpi("TRIG")]
        Trigger_and_Run,
        /// <summary>
        /// Play continuously at first trigger. Subsequent triggers reset the segment and play it continuously.
        /// </summary>
        [Scpi("RES")]
        Reset_and_Run
    }

    /// <summary>
    /// Options for the Single trigger type
    /// </summary>
    public enum TriggerSingleEnum
    {
        /// <summary>
        /// If the waveform is triggered while it is playing, it plays to the end and then plays once more.
        /// </summary>
        [Scpi("ON")]
        Buffered_Trigger,
        /// <summary>
        /// If the waveform is triggered while it is playing, the trigger is ignored.
        /// </summary>
        [Scpi("OFF")]
        No_Retrigger,
        /// <summary>
        /// If the waveform is triggered while it is playing, the waveform resets and replays from the start.
        /// </summary>
        [Scpi("IMM")]
        Restart_On_Trigger
    }

    /// <summary>
    /// Options for the Gated trigger type
    /// </summary>
    public enum TriggerGatedEnum
    {
        /// <summary>
        /// Waveform playback starts when trigger goes low and stops when trigger goes high.
        /// </summary>
        [Scpi("LOW")]
        Active_Low,
        /// <summary>
        /// Waveform playback starts when trigger goes high and stops when trigger goes low.
        /// </summary>
        [Scpi("HIGH")]
        Active_High
    }

    /// <summary>
    /// Options for the Segment Advance trigger type
    /// </summary>
    public enum TriggerSegAdvEnum
    {
        /// <summary>
        /// If the waveform is triggered while it is playing, it plays to the end and the next segment plays once.
        /// </summary>
        [Scpi("SING")]
        Single,
        /// <summary>
        /// If the waveform is triggered while it is playing, it plays to the end and the next segment plays continuously.
        /// </summary>
        [Scpi("CONT")]
        Continuous
    }

    /// <summary>
    /// Trigger Source Options
    /// </summary>
    public enum TriggerSourceEnum
    {
        /// <summary>
        /// The Trigger front panel hard key.
        /// </summary>
        [Scpi("KEY")]
        Trigger_Key,
        /// <summary>
        /// An external source applied to rear-panel connectors (as chosen by External Trigger Source setting).
        /// </summary>
        [Scpi("EXT")]
        External,
        /// <summary>
        /// A trigger command sent over GPIB, USB, or LAN.
        /// </summary>
        [Scpi("BUS")]
        Bus,
    }

    /// <summary>
    /// Routing options for the External trigger source
    /// </summary>
    public enum TriggerExtSourceEnum
    {
        /// <summary>
        /// No external connector
        /// </summary>
        [Scpi("NONE")]
        None,
        /// <summary>
        /// Baseband trigger 1
        /// </summary>
        [Scpi("BBTR1")]
        BB_Trigger_1,
        /// <summary>
        /// Baseband trigger 2
        /// </summary>
        [Scpi("BBTR2")]
        BB_Trigger_2,
        /// <summary>
        /// Event 1
        /// </summary>
        [Scpi("EVEN1")]
        Event_1,
        /// <summary>
        /// Pattern Trigger
        /// </summary>
        [Scpi("PTR")]
        Pattern_Trigger
    }

    /// <summary>
    /// Memory type options
    /// </summary>
    public enum MemoryEnum
    {
        /// <summary>
        /// Store to Volatile memory
        /// </summary>
        Volatile,
        /// <summary>
        /// Store to Non-Volatile memory
        /// </summary>
        NonVolatile
    }
}
