﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using Keysight.Tap.Plugins.OpenCellular.Duts;

namespace Keysight.Tap.Plugins.OpenCellular.TestSteps
{
    [Display("Set Rx Attenuation", Groups: new[] { "OpenCellular", "Osmo DUT Control" })]
    public class SetRxAttenuation : TestStep
    {
        #region Settings
        [Display("Osmo BTS Device", Description: "The DUT to be controlled", Order: 1)]
        public OsmoBts Bts { get; set; }
        [Display("Selected Tx/Rx Port", Description: "Chooses which port to enable and set parameters for", Group: "I/O", Order: 1)]
        public int SelectedTrx { get; set; }
        [Display("Rx Attenuation", Description: "Number of decibels of attenuation for the recieve path", Group: "I/O", Order: 3)]
        [Unit("dB")]
        public int RxAttenuation { get; set; }
        #endregion

        public SetRxAttenuation()
        {
            SelectedTrx = 0;
            RxAttenuation = 0;
        }

        public override void Run()
        {
            Bts.SetRxAttenuation(SelectedTrx, RxAttenuation);
        }
    }
}
