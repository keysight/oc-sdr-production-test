﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer;
using Renci.SshNet;
using static System.Threading.Thread;
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Keysight.Tap.Plugins.OpenCellular.Duts
{
    [Display("Osmo BTS DUT", Group: "OpenCellular")]
    [ShortName("OsmoBTS")]
    // ReSharper disable once ClassNeverInstantiated.Global
    public class OsmoBts : Dut
    {
        private string clock = "";
        private SshClient client;
        private bool externalClock;
        private StreamHelper stream;

        #region Settings

        [Display("Address", Description: "IP Address of Osmo BTS device", Order: 1)]
        public string Address { get; set; }

        [Display("Port", Description: "Port to use for SSH-based communication to the device. Default is 22", Group: "SSH", Order: 5)]
        public int Port { get; set; }

        [Display("User Name", Description: "Linux Log-in User Name of Osmo BTS device", Group: "SSH", Order: 1)]
        public string Username { get; set; }

        [Display("Password", Description: "Linux Log-in Password of Osmo BTS device", Group: "SSH", Order: 2)]
        public string Password { get; set; }

        [Display("Number of Tx/Rx Ports", Description: "The number of transmit and receive ports supported by the device", Group: "I/O", Order: 1)]
        public int NumberOfTrxPorts { get; set; }

        [Display("Linux User", Description: "The user who has access to the Osmo API. Required for creating a connection", Group: "SSH", Order: 4)]
        public string User { get; set; }

        [Display("Osmo API Path", Description: "Path to the Osmo_API.py interface", Group: "SSH", Order: 3)]
        public string OsmoPath { get; set; }

        [Display("Use External Clock", Description: "Sets clock source of device to the external 10MHz reference port", Group: "I/O", Order: 3)]
        public bool ExternalClock
        {
            get { return externalClock; }
            set
            {
                externalClock = value;
                clock = externalClock ? "-x " : "";
            }
        }
        #endregion

        /// <summary>
        /// Initializes a new instance of this DUT class.
        /// </summary>
        public OsmoBts()
        {
            NumberOfTrxPorts = 1;
            User = "opencellular";
            ExternalClock = true;
            OsmoPath = "/home/opencellular";
            Port = 22;
        }

        /// <summary>
        /// Opens a connection to the DUT represented by this class
        /// </summary>
        public override void Open()
        {
            base.Open();
            client = new SshClient(Address, Port, Username, Password);
            client.Connect();
            stream = new StreamHelper(client, OsmoPath, Username, Log);
            IsConnected = true;
            
            var command = "sudo /home/oc/host/ocmw/ocmw_usb_revc &";
            var result = client.RunCommand(command);
            Log.Info("Command > " + command);
            Log.Info("Result > " + result.Result);
        }

        /// <summary>
        /// Closes the connection made to the DUT represented by this class
        /// </summary>
        public override void Close()
        {
            stream.Close();
            client.Disconnect();
            base.Close();
        }

        public void CreateConnectionAndInitialize(int selectedTrx, int arfcn, int txAttenuation, int rxAttenuation, bool enableEdge)
        {
            var edge = enableEdge ? "-e " : "";
            Log.Info("Connecting to and initializing DUT");
            stream.SendInitCommand($"-n {NumberOfTrxPorts} -T {selectedTrx} -a {arfcn} -t {txAttenuation} -r {rxAttenuation} -i {edge}{clock}");
        }

        public void ResetFx3()
        {
            Log.Info("Resetting FX3.....");
            const string command = "/home/oc/host/ocmw/occmd reset sdr.fx3";
            var result = client.RunCommand(command);
            Log.Info("Command > " + command);
            Log.Info("Result > " + result.Result);
        }

        public void Initialize(int selectedTrx, int arfcn, int txAttenuation, int rxAttenuation)
        {
            Log.Info("Initializing DUT");
            SendOsmoCommand($"-n {NumberOfTrxPorts} -T {selectedTrx} -a {arfcn} -t {txAttenuation} -r {rxAttenuation} {clock}");
        }

        public void SetTxAttenuation(int selectedTrx, int txAttenuation)
        {
            Log.Info("Setting Tx Attenuation = " + txAttenuation);
            SendOsmoCommand($"-n {NumberOfTrxPorts} -T {selectedTrx} -t {txAttenuation} {clock}");
        }

        public void SetRxAttenuation(int selectedTrx, int rxAttenuation)
        {
            Log.Info("Setting Rx Attenuation = " + rxAttenuation);
            SendOsmoCommand($"-n {NumberOfTrxPorts} -T {selectedTrx} -r {rxAttenuation} {clock}");
        }

        public void SetArfcn(int selectedTrx, int arfcn)
        {
            Log.Info("Setting ARFCN = " + arfcn);
            SendOsmoCommand($"-n {NumberOfTrxPorts} -T {selectedTrx} -a {arfcn} {clock}");
        }

        public void Kill()
        {
            Log.Info("Killing Osmo Stack!");
            SendOsmoCommand("-k osmo-");
        }

        public void StartStack()
        {
            Log.Info("Starting Osmo Stack");
            SendOsmoCommand(""); //No arguments
        }

        public string GetBer(BmtEnum bmt, AntennaEnum antenna)
        {
            Log.Info("Sending BER request to DUT " + Name);
            stream.SendCommand(
                $"/usr/local/osmo_built/bin/osmo-nitb -c {GetBerBandFile(bmt)} -l ./../osmo_cfg/hlr.sqlite3 -P -C --debug=DRLL:DCC:DMM:DRR:DRSL:DNM");
            stream.SendCommand("/usr/local/osmo_built/bin/osmo-bts-trx -c ~/rx_test/osmo_cfg/osmo-bts.cfg -t 1");
            stream.SendCommand($"/usr/local/osmo_built/bin/osmo-trx -c 1 {((antenna == AntennaEnum.Antenna2) ? "-S" : "")} -x");
            stream.SendCommand("cd  ~/rx_test/osmo-run-script/", 1);
            SendCliCommand("python transceiver_recording_mask2.py -f ber_rx_sample 255;sleep 1;");
            SendCliCommand("python transceiver_recording_mask2.0;");
            stream.SendCommand("cd  ~/rx_test/osmo-run-script/", 1);
            return stream.SendCommand("./osmo-trx-dec -s 1 -t 0 -f ber_rx_sample -b 10");
        }

        private string GetBerBandFile(BmtEnum bmt)
        {
            switch (bmt)
            {
                case BmtEnum.Bottom:
                    return "~/rx_test/osmo_cfg/osmo-nitb_bottom.cfg";
                case BmtEnum.Middle:
                    return "~/rx_test/osmo_cfg/osmo-nitb_middle.cfg";
                case BmtEnum.Top:
                    return "~/rx_test/osmo_cfg/osmo-nitb_top.cfg";
                default:
                    throw new ArgumentOutOfRangeException(nameof(bmt), bmt, null);
            }
        }

        private void SendOsmoCommand(string arguments)
        {
            string path = OsmoPath.EndsWith("/") ? OsmoPath : OsmoPath + "/";
            var command = $"python3 {path}osmo_API.py {arguments}";
            SendCliCommand(command);
        }

        private void SendCliCommand(string command)
        {
            var result = client.RunCommand(command);

            Log.Info("Command > " + command);
            Log.Info("Result > " + result.Result);
        }
    }

    public class StreamHelper
    {
        private readonly StreamReader reader;
        private readonly StreamWriter writer;
        private readonly string osmoPath;
        private readonly string username;
        private readonly TraceSource log;
        
        public StreamHelper(SshClient client, string osmoPath, string username, TraceSource log)
        {
            this.osmoPath = osmoPath;
            this.username = username;
            this.log = log;
            
            var shell = client.CreateShellStream("terminal", 200, 24, 800, 600, 1024);
            writer = new StreamWriter(shell);
            reader = new StreamReader(shell);

            writer.AutoFlush = true;
        }
        
        public string SendCommand(string command, int waitTime = 6000, params string[] endLines)
        {
            writer.WriteLine(command);

            StringBuilder result = new StringBuilder();

            var watch = new Stopwatch();
            watch.Start();
            while (watch.ElapsedMilliseconds < waitTime)
            {
                string line = reader.ReadLine();
                if (string.IsNullOrEmpty(line))
                {
                    Sleep(500);
                    continue;
                }

                foreach (string endLine in endLines)
                {
                    if (line.Trim() == endLine)
                        break;
                }
                log.Info(line);
                result.AppendLine(line);
            }

            return result.ToString();
        }
        
        public void SendInitCommand(string arguments)
        {
            string path = osmoPath.EndsWith("/") ? osmoPath : osmoPath + "/";
            var command = $"python3 {path}osmo_API.py {arguments}";
            
            SendCommand(command, 40000, $"{username}@Connect1:~#", $"{username}@Connect1:~$");
        }

        public void Close()
        {
            reader.Close();
            writer.Close();
        }
    }

    public enum AntennaEnum
    {
        Antenna1,
        Antenna2
    }

    public enum BmtEnum
    {
        Bottom,
        Middle,
        Top
    }
}
