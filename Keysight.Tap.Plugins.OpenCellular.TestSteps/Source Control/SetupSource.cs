﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using Keysight.Tap.Plugins.OpenCellular.XSeriesGenerator;
using static Keysight.Tap.ScpiInstrument;

namespace Keysight.Tap.Plugins.OpenCellular.TestSteps
{
    [Display("Setup Source", Groups: new[] { "OpenCellular", "Source Control" })]
    public class SetupSource : TestStep
    {
        #region Settings
        [Display("VSG", Order: 0.1)]
        public XSeriesGenerator.XSeriesGenerator Vsg { get; set; }

        [Display("Trigger Type", Group: "Arb Setup", Order: 60.1)]
        public SourceTriggerEnum Trigger { get; set; }

        [Display("Continuous Trigger", Group: "Arb Setup", Order: 60.2, Description: "Free Run - Play continuously without waiting for a trigger. \nTrigger and Run - Play continuously at first trigger and ignore subsequent triggers. \nReset and Run - Play continuously at first trigger. Subsequent triggers reset the segment and play it continuously.")]
        [EnabledIf("Trigger", SourceTriggerEnum.Continuous)]
        public TriggerContinuousEnum TriggerContinuous { get; set; }

        [Display("Single Trigger", Group: "Arb Setup", Order: 60.3, Description: "Buffered Trigger - If the waveform is triggered while it is playing, it plays to the end and then plays once more. \nNo Retrigger - If the waveform is triggered while it is playing, the trigger is ignored. \nRestart on Trigger - If the waveform is triggered while it is playing, the waveform resets and replays from the start.")]
        [EnabledIf("Trigger", SourceTriggerEnum.Single)]
        public TriggerSingleEnum TriggerSingle { get; set; }

        [Display("Gated Trigger", Group: "Arb Setup", Order: 60.4, Description: "Active Low - Waveform playback starts when trigger goes low and stops when trigger goes high. \nActive High - Waveform playback starts when trigger goes high and stops when trigger goes low.")]
        [EnabledIf("Trigger", SourceTriggerEnum.Gated)]
        public TriggerGatedEnum TriggerGated { get; set; }

        [Display("Segment Advance Trigger", Group: "Arb Setup", Order: 60.5, Description: "Single - If the waveform is triggered while it is playing, it plays to the end and the next segment plays once. \nContinuous - If the waveform is triggered while it is playing, it plays to the end and the next segment plays continuously.")]
        [EnabledIf("Trigger", SourceTriggerEnum.Segment_Advance)]
        public TriggerSegAdvEnum TriggerSegAdv { get; set; }

        [Display("Trigger Source", Group: "Arb Setup", Order: 60.6, Description: "Sets trigger source to: \nTrigger Key - The Trigger front panel hard key. \nExternal - An external source applied to rear-panel connectors (as chosen by External Trigger Source setting). \nBus - A trigger command sent over GPIB, USB, or LAN.")]
        public TriggerSourceEnum TriggerSource { get; set; }

        [Display("External Trigger Source", Group: "Arb Setup", Order: 60.7, Description: "Sets which rear-panel connector routes to the internal Pattern Trigger 1 port for external triggering")]
        [EnabledIf("TriggerSource", TriggerSourceEnum.External)]
        public TriggerExtSourceEnum TriggerExtSource { get; set; }

        [Display("Runtime Scaling", Group: "Arb Setup", Order: 60.8)]
        [Unit("%")]
        public double RuntimeScaling { get; set; }

        [Display("Waveform File Name", Group: "Arb Setup", Order: 60.91, Description: "Name of the waveform to be played located in the Source Volatile or Non-Volatile memory. \nIf a waveform of that name doesn't exist, the file selected under Waveform Path will be uploaded to the Source under this name.")]
        public string FileName { get; set; }

        [Display("Waveform File Path", Group: "Arb Setup", Order: 60.92, Description: "The location of a waveform file on the local disk to be loaded to the Source. \nOnly active if no waveform with the name 'Waveform Name' exists on the Source")]
        [FilePath]
        public string FilePath { get; set; }

        [Display("Modulation On/Off", Group: "General", Order: 50.0)]
        public bool Modulation { get; set; }

        [Display("RF On/Off", Group: "General", Order: 50.1)]
        public bool Rf { get; set; }

        [Display("Center Frequency", Group: "General", Order: 50.2)]
        [Unit("Hz", true)]
        public double Frequency { get; set; }

        [Display("Amplitude", Group: "General", Order: 50.3)]
        [Unit("dBm")]
        public double Amplitude { get; set; }
        #endregion

        //Called when adding step to test plan
        public SetupSource()
        {
            // Set default values of parameters
            Trigger = SourceTriggerEnum.Continuous;
            TriggerContinuous = TriggerContinuousEnum.Free_Run;
            TriggerSource = TriggerSourceEnum.External;
            TriggerExtSource = TriggerExtSourceEnum.Pattern_Trigger;
            Modulation = true;
            Rf = true;
            Frequency = 6E9;
            Amplitude = -144;
            RuntimeScaling = 70;
        }

        public override void Run()
        {
            //Set output waveform
            Vsg.SetupWaveform(FileName, FilePath);

            //Propagate TAP GUI values to VSG
            Vsg.Frequency = Frequency;
            Vsg.Amplitude = Amplitude;
            Vsg.Modulation = Modulation;
            Vsg.Trigger = Trigger;
            Vsg.TriggerContinuous = TriggerContinuous;
            Vsg.TriggerSingle = TriggerSingle;
            Vsg.TriggerGated = TriggerGated;
            Vsg.TriggerSegAdv = TriggerSegAdv;
            Vsg.TriggerSource = TriggerSource;
            Vsg.TriggerExtSource = TriggerExtSource;
            Vsg.RuntimeScaling = RuntimeScaling;
            Vsg.Rf = Rf;
            Vsg.Apply();

            var errors = Vsg.QueryErrors();

            foreach (ScpiError err in errors)
                Log.Error(Vsg.Name + " Error: " + err.Message);

            if (errors.Count != 0)
                UpgradeVerdict(Verdict.Error);
        }
    }
}
