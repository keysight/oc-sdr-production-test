﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer;
using static Keysight.Tap.ScpiInstrument;

namespace Keysight.Tap.Plugins.OpenCellular.TestSteps
{
    [Display("Output RF Spectrum", Groups: new[] { "OpenCellular", "Measurements" }, Description: "The Output RF Spectrum measurement is the GSM version of the adjacent channel power (ACP) measurement.")]
    public class Orfs : TxBase
    {
        #region Settings
        [Display("Averaging", Group: "Meas Setup", Order: 70.0, Description: "Sets the number of bursts that are averaged. After the specified number of bursts (average counts), \nthe averaging mode (termination control) setting determines the averaging action.")]
        public Enabled<int> Averaging { get; set; }

        [Display("Trigger Source", Group: "Trigger", Order: 10.1, Collapsed: true, Description: "Select trigger source: \nFree Run - Triggering occurs immediately after the sweep/measurement is initiated. \nVideo - Trigger condition is met when the video signal (the filtered and detected version of the input signal, including both RBW and VBW filtering) crosses the video trigger level. \nLine - A new sweep/measurement will start synchronized with the next cycle of the line voltage. \nExternal 1 - A new sweep/measurement will start when the external trigger condition is met using the external 1 input connector on the rear panel. \nExternal 2 - A new sweep/measurement will start when the external trigger condition is met using the external 2 input connector on the rear panel. \nRF Burst - A new sweep/measurement will start when an RF burst envelope signal is identified from the signal at the RF Input connector.")]
        public VsaTriggerEnum TriggerSource { get; set; }

        [Display("Measurement Type", Group: "Meas Setup", Order: 70.2, Description: "Mod and Switch - Performs both Modulation and Switching measurements. \nModulation - Measures the spectrum due to the 3/8pi shift 8PSK modulation and noise. \nSwitching - Measures the spectrum due to switching transients (burst ramping). \nFull Frame Mod - Improves measurement speed by acquiring a full frame of data prior to performing the FFT calculation. This feature can only be used when all slots in the transmitted frame are active.")]
        public OutputRfSpectrumMeasEnum MeasurementType { get; set; }

        [Display("Measurement Method", Group: "Meas Setup", Order: 70.3, Description: "Multi Offset - The measurement is done at all offsets in the offset frequency list. \nSwept - The measurement is done in the frequency domain. For output RF spectrum due to modulation it is done using time-gated spectrum analysis to sweep the analyzer with the gate turned on for the desired portion of the burst only.")]
        public OutputRfSpectrumMeasMethodEnum MeasMethod { get; set; }

        [Display("Offset Frequency List", Group: "Meas Setup", Order: 70.4, Description: "Select the type of a frequency list used for Multi Offset. \nStandard - the complete list of the offset frequencies specified in the GSM Standards, except for those offsets greater than 6 MHz. \nShort - a shortened list of the offset frequencies specified in the GSM Standards. \n")]
        [EnabledIf("MeasMethod", OutputRfSpectrumMeasMethodEnum.Multi_Offset)]
        public OutputRfSpectrumOffsetFrequencyListEnum OffsetFrequencyList { get; set; }

        [Display("Wideband Noise", Group: "Meas Setup", Order: 70.5)]
        [EnabledIf("MeasMethod", OutputRfSpectrumMeasMethodEnum.Swept)]
        public bool WidebandNoise { get; set; }

        [Display("Mode", Group: "Radio", Order: 20.0, Description: "Select measurement mode.")]
        public ModeEnum Mode { get; set; }
        #endregion

        private string results;
        private IOutputRfSpectrumMeasurement outputRfSpectrum;
        private Measurements measurement;

        public Orfs()
        {
            Averaging = new Enabled<int>() { IsEnabled = true, Value = 20 };
            TriggerSource = VsaTriggerEnum.RF_Burst;
            MeasurementType = OutputRfSpectrumMeasEnum.Modulation;
            MeasMethod = OutputRfSpectrumMeasMethodEnum.Multi_Offset;
            OffsetFrequencyList = OutputRfSpectrumOffsetFrequencyListEnum.Short;
            WidebandNoise = false;
            Name = "{Mode} Output Rf Spectrum";
            Mode = ModeEnum.GSM;
        }

        public override void Run()
        {
            base.Run();

            switch (Mode)
            {
                case ModeEnum.GSM:
                    outputRfSpectrum = Vsa.GmskOutputRfSpectrum;
                    measurement = Measurements.GmskOutputRfSpectrum;
                    break;
                case ModeEnum.EDGE:
                    outputRfSpectrum = Vsa.EdgeOutputRfSpectrum;
                    measurement = Measurements.EdgeOutputRfSpectrum;
                    break;
                default:
                    throw new Exception("Unexpected Mode Type");
            }

            outputRfSpectrum.AverageCount = Averaging.Value;
            outputRfSpectrum.Averaging = Averaging.IsEnabled;
            outputRfSpectrum.MeasMethod = MeasMethod;
            outputRfSpectrum.MeasurementType = MeasurementType;
            outputRfSpectrum.OffsetFrequencyList = OffsetFrequencyList;
            outputRfSpectrum.TriggerSource = TriggerSource;
            outputRfSpectrum.WidebandNoise = WidebandNoise;

            Vsa.Apply(measurement);

            results = Vsa.Measure(measurement);

            var errors = Vsa.QueryErrors();
            foreach (ScpiError err in errors)
                Log.Error(Vsa.Name + " Error: " + err.Message);

            UpgradeVerdict(Vsa.Verdict() ? Verdict.Pass : Verdict.Fail);

            LogData();
        }

        private void LogData()
        {
            List<string> resultNames = new List<string>();
            List<string> emptyList = new List<string> { "", "", "", "" };

            var res = results.Split(',');
            double[] values = new double[res.Length];

            for (int i = 0; i < res.Length; i++)
            {
                values[i] = double.Parse(res[i]);
            }

            bool reportMultiOffset = false;
            bool reportSwitching = false;

            switch (outputRfSpectrum.MeasMethod)
            {
                case OutputRfSpectrumMeasMethodEnum.Multi_Offset:
                    for (int i = 0; i < 15; i++)
                    {
                        if (values[4 * i] == -999 && values[4 * i + 1] == -999 && values[4 * i + 2] == -999 && values[4 * i + 3] == -999)
                            resultNames.AddRange(emptyList);
                        else
                        {
                            resultNames.AddRange(new List<string> {
                                $"Offset {i}L - Power Relative to Carrier (dB)",
                                $"Offset {i}L - Absolute Average Power (dBm)",
                                $"Offset {i}U - Power Relative to Carrier (dB)",
                                $"Offset {i}U - Absolute Average Power (dBm)"
                            });
                            reportMultiOffset = true;
                        }
                    }

                    if (reportMultiOffset)
                        Log.Info("------ Modulation Measurement Results ------");
                    LogScpiValues(resultNames, results);
                    PublishScpiValues("Output Rf Spectrum - Modulation Measurement", resultNames, results);

                    resultNames.Clear();
                    for (int i = 0; i < 15; i++)
                    {
                        resultNames.AddRange(emptyList);
                    }

                    for (int i = 15; i < 30; i++)
                    {
                        if (values[4 * i] == -999 && values[4 * i + 1] == -999 && values[4 * i + 2] == -999 && values[4 * i + 3] == -999)
                            resultNames.AddRange(emptyList);
                        else
                        {
                            resultNames.AddRange(new List<string> {
                                $"Offset {i - 15}L - Power Relative to Carrier (dB)",
                                $"Offset {i - 15}L - Absolute Average Power (dBm)",
                                $"Offset {i - 15}U - Power Relative to Carrier (dB)",
                                $"Offset {i - 15}U - Absolute Average Power (dBm)"
                            });

                            reportSwitching = true;
                        }
                    }

                    if (reportSwitching)
                        Log.Info("------ Switching Transients Measurement Results ------");
                    LogScpiValues(resultNames, results);
                    PublishScpiValues("Output Rf Spectrum - Switching Transients Measurement", resultNames, results);
                    break;
                    
                case OutputRfSpectrumMeasMethodEnum.Swept:
                    resultNames = new List<string> { "Frequency (Hz)", "Offset from Carrier (Hz)", "Power (dBm)", "Delta from Limit (dB)", "Delta from Reference (dB)" };
                    Log.Info("------ Results of the closest point to the limit line");
                    LogScpiValues(resultNames, results);
                    PublishScpiValues("Output Rf Spectrum - Swept", resultNames, results);
                    break;
            }
        }

    }
}
