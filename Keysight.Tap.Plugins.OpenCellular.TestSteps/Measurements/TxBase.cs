﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System;
using System.Collections.Generic;
using Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer;

namespace Keysight.Tap.Plugins.OpenCellular.TestSteps
{
    public abstract class TxBase : TestStep
    {
        #region Settings
        [Display("VSA", Order: 0.0)]
        public XSeriesAnalyzer.XSeriesAnalyzer Vsa { get; set; }

        [Display("Frequency Selection Preference", Group: "Frequency", Order: .05)]
        public FrequencySelectionEnum FrequencySelection { get; set; }

        [Display("VSA Center Frequency", Group: "Frequency", Order: 0.2)]
        [Unit("Hz", true)]
        [EnabledIf("FrequencySelection", FrequencySelectionEnum.Center_Frequency)]
        public double Frequency { get; set; }

        [Display("Auto Trigger", Group: "Trigger", Order: 10.0, Collapsed: true, Description: "Sets the time that the analyzer will wait for the trigger conditions to be met. \nIf they are not met after that much time, then the analyzer is triggered anyway.")]
        [Unit("ms")]
        public Enabled<double> AutoTrigger { get; set; }

        [Display("Band", Group: "Radio", Order: 20.05, Description: "Selects the standard variant that applies to the radio to be tested.")]
        public BandEnum Band { get; set; }

        [Display("Device", Group: "Radio", Order: 20.1, Description: "Selects the type of radio device to be tested. \nBTS - Base station transmitter test \nMS - Mobile station transmitter test")]
        public RadioDeviceEnum RadioDevice { get; set; }

        [Display("BTS Type", Group: "Radio", Order: 20.2, Description: "Selects the type of base station to be tested.")]
        public BtsEnum BtsEnum { get; set; }

        [Display("Mechanical Attenuation", Group: "Attenuation", Order: 30.0, Collapsed: true, Description: "Modify the attenuation applied to the RF input signal path.")]
        [Unit("dB")]
        public int MechAtten { get; set; }

        [Display("Electrical Attenuation", Group: "Attenuation", Order: 30.2, Collapsed: true, Description: "Enable/Adjust the electronic attenuator. \nThe electronic attenuator offers finer steps than the mechanical attenuator, has no acoustical noise, is faster, and is less subject to wear.")]
        [Unit("dB")]
        public Enabled<int> ElecAtten { get; set; }

        [Display("Pre-Adjust for Min Clip", Group: "Attenuation", Order: 30.3, Collapsed: true, Description: "Determine whether to perform an AdjustAttenMinClip() each time a measurement starts.")]
        public MinClipEnum MinClip { get; set; }

        [Display("Automatic Alignment", Group: "System", Order: 40.0, Collapsed: true, Description: "Configures the method for which the automatic background alignment is run. \nAutomatic background alignments are run periodically between measurement acquisitions. The instrument’s software determines when alignments are to be performed to maintain warranted operation. \nThe recommended setting for Auto Align is Normal.")]
        public AutoAlignEnum AutoAlign { get; set; }

        [Display("External BTS Gain", Group: "Input/Output", Order: 15.0, Collapsed: true, Description: "Sets the external gain/attenuation value for MS (Mobile Station) tests")]
        public double ExtGainBts { get; set; }

        [Display("External MS Gain", Group: "Input/Output", Order: 15.1, Collapsed: true, Description: "Sets the external gain/attenuation value for BTS (Base Transceiver Station) tests")]
        public double ExtGainMs { get; set; }

        [Display("ARFCN", Group: "Frequency", Order: 0.4, Description: "Sets the Absolute RF Channel Number")]
        [EnabledIf("FrequencySelection", FrequencySelectionEnum.ARFCN)]
        public int Arfcn { get; set; }

        [Display("BMT Frequency", Group: "Frequency", Order: 0.3, Description: "Selects a Channel Number Preset")]
        [EnabledIf("FrequencySelection", FrequencySelectionEnum.BMT_Frequency)]
        public BmtFreqEnum BmtFreq { get; set; }

        [Display("Time Slot", Group: "Frequency", Order: 0.5, Description: "Selects On or Off for slot searching. Generally, this feature is only valid in external and periodic timer trigger source modes \nthat triggers every frame since another trigger source does not have the information that identifies the head of the frame. \nWhen Timeslot is set to On, the demodulation measurement is made on the nth timeslot specified by the \ntrigger point + n timeslots, where n is the selected timeslot value 0 to 7.")]
        public Enabled<int> TimeSlot { get; set; }

        [Display("Burst Type", Group: "Frequency", Order: 0.6, Description: "Sets the burst type that the analyzer will search for and to which it will synchronize.")]
        public BurstTypeEnum BurstType { get; set; }

        [Display("TSC (Std)", Group: "Frequency", Order: 0.7, Description: "Allows you to select the Training Sequence Code that determines which burst is to be measured. \nApplicable only when Burst Sync is set to Training Sequence in the measurement. \nIf disabled, measurement will use Automatic Detection and the measurement will be made on the first burst found to have a valid TSC in the range from 0 to 7.")]
        public Enabled<int> Tsc { get; set; }
        #endregion

        //Called when adding step to test plan
        protected TxBase()
        {
            // Set default values of parameters
            Frequency = 935.2E6;
            Band = BandEnum.P_GSM;
            RadioDevice = RadioDeviceEnum.BTS;
            BtsEnum = BtsEnum.Normal;
            MechAtten = 10;
            ElecAtten = new Enabled<int>() { IsEnabled = true, Value = 0 };
            MinClip = MinClipEnum.Off;
            AutoTrigger = new Enabled<double>() { IsEnabled = false, Value = 100 };
            AutoAlign = AutoAlignEnum.Normal;
            ExtGainBts = 0;
            ExtGainMs = 0;
            Arfcn = 1;
            BmtFreq = BmtFreqEnum.Bottom;
            TimeSlot = new Enabled<int>() { IsEnabled = false, Value = 0 };
            BurstType = BurstTypeEnum.Normal;
            Tsc = new Enabled<int>() { IsEnabled = true, Value = 0 };
        }

        public override void Run()
        {
            //Ensure GSM application is running
            Vsa.SetMode(Modes.GSM);
            Vsa.WaitForOperationComplete();

            //Propagate TAP GUI values to VSA
            Vsa.Frequency = Frequency;
            Vsa.FrequencySelection = FrequencySelection;
            Vsa.AutoTrigger = AutoTrigger.IsEnabled;
            Vsa.AutoTriggerTime = AutoTrigger.Value;
            Vsa.Band = Band;
            Vsa.RadioDevice = RadioDevice;
            Vsa.Bts = BtsEnum;
            Vsa.MechAtten = MechAtten;
            Vsa.ElecAttenEnable = ElecAtten.IsEnabled;
            Vsa.ElecAtten = ElecAtten.Value;
            Vsa.MinClip = MinClip;
            Vsa.ExtGainMs = ExtGainMs;
            Vsa.ExtGainBts = ExtGainBts;
            Vsa.AutoAlign = AutoAlign;
            Vsa.BmtFreq = BmtFreq;
            Vsa.Arfcn = Arfcn;
            Vsa.TimeSlot = TimeSlot.Value;
            Vsa.TimeSlotEnabled = TimeSlot.IsEnabled;
            Vsa.BurstType = BurstType;
            Vsa.TscEnabled = !Tsc.IsEnabled;
            Vsa.Tsc = Tsc.Value;
        }

        /// <summary>
        /// Helper function built around Log.Info that enables easy logging of a SCPI query result string 
        /// (comma delimited) by one-to-one association with a List of result name strings
        /// </summary>
        protected void LogScpiValues(List<string> resultNames, string resultScpi)
        {
            double[] values = ParseCommaDelimitedStringToDoubles(resultScpi);

            int len = values.Length < resultNames.Count ? values.Length : resultNames.Count;
            for (int i = 0; i < len; i++)
            {
                if (resultNames[i] == string.Empty)
                    continue;
                Log.Info(resultNames[i] + ": " + values[i]);
            }
        }

        /// <summary>
        /// Helper function built around Results.Publish that enables easy publishing of a SCPI query result string 
        /// (comma delimited) by one-to-one association with a List of result name strings
        /// </summary>
        protected void PublishScpiValues(string name, List<string> resultNames, string resultScpi)
        {

            double[] values = ParseCommaDelimitedStringToDoubles(resultScpi);

            double[][] baseArray = new double[][] { values };
            Array[] array = FlipArrayDims(baseArray);

            Results.PublishTable(name, resultNames, array);
        }


        protected double[] ParseCommaDelimitedStringToDoubles(string commaDelimitedString)
        {
            string[] strings = commaDelimitedString.Split(',');
            double[] doubles = new double[strings.Length];

            for (int i = 0; i < strings.Length; i++)
            {
                doubles[i] = double.Parse(strings[i]);
            }

            return doubles;
        }

        /// <summary>
        /// Flips dimensions of an array. Used specifically by PublishScpiValues() in order to bypass Results.Publish() 
        /// params input, simplifying process of publishing single values (as returned from SCPI Queries)
        /// </summary>
        public T[][] FlipArrayDims<T>(T[][] baseArray)
        {
            int newRows = baseArray.Length;
            int newCols = baseArray[0].Length;

            T[][] output = new T[newCols][];
            for (int i = 0; i < newCols; i++)
            {
                output[i] = new T[newRows];
                for (int j = 0; j < newRows; j++)
                {
                    output[i][j] = baseArray[j][i];
                }
            }

            return output;
        }
    }
}
