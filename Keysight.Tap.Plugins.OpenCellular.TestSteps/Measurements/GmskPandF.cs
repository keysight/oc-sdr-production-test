﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System.Collections.Generic;
using Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer;
using static Keysight.Tap.ScpiInstrument;

namespace Keysight.Tap.Plugins.OpenCellular.TestSteps
{
    [Display("GMSK Phase and Frequency", Groups: new[] { "OpenCellular", "Measurements" }, Description: "Phase and frequency error are the measures of modulation quality for GSM systems. Since GSM systems use relative phase to transmit information, the phase and frequency accuracy of the transmitter are critical to the systems performance and ultimately affect range.")]
    public class GmskPandF : TxBase
    {
        #region Settings

        [Display("Averaging", Group: "Meas Setup", Order: 70.0, Description: "Sets the number of bursts that are averaged. After the specified number of bursts (average counts), \nthe averaging mode (termination control) setting determines the averaging action.")]
        public Enabled<int> Averaging { get; set; }

        [Display("Trigger Source", Group: "Trigger", Order: 10.1, Collapsed: true, Description: "Select trigger source: \nFree Run - Triggering occurs immediately after the sweep/measurement is initiated. \nVideo - Trigger condition is met when the video signal (the filtered and detected version of the input signal, including both RBW and VBW filtering) crosses the video trigger level. \nLine - A new sweep/measurement will start synchronized with the next cycle of the line voltage. \nExternal 1 - A new sweep/measurement will start when the external trigger condition is met using the external 1 input connector on the rear panel. \nExternal 2 - A new sweep/measurement will start when the external trigger condition is met using the external 2 input connector on the rear panel. \nRF Burst - A new sweep/measurement will start when an RF burst envelope signal is identified from the signal at the RF Input connector.")]
        public VsaTriggerEnum TriggerSource { get; set; }

        [Display("Average Mode", Group: "Meas Setup", Order: 70.3, Description: "Select the type of termination control used for the averaging function. This determines the averaging \naction after the specified number of data acquisitions (average count) is reached.")]
        public AverageModeEnum AverageMode { get; set; }

        [Display("Burst Sync", Group: "Meas Setup", Order: 70.4, Description: "Allows you to select the method of synchronizing the measurement to the bursts. \nRF Amplitude – The burst synchronization approximates the start and stop of the useful part of the burst without demodulation of the burst. \nTraining Seq – The burst synchronization performs a demodulation of the burst and determines the start and stop of the useful part of the burst based on the midamble training sync sequence.")]
        public GmskPhaseAndFrequencyBurstSyncEnum BurstSync { get; set; }

        [Display("Display Mode", Group: "View/Display", Order: 80.0, Collapsed: true)]
        public GmskPhaseAndFrequencyDisplayEnum Display { get; set; }

        #endregion

        public GmskPandF()
        {
            Averaging = new Enabled<int>() { IsEnabled = false, Value = 10 };
            TriggerSource = VsaTriggerEnum.RF_Burst;
            AverageMode = AverageModeEnum.Repeat;
            BurstSync = GmskPhaseAndFrequencyBurstSyncEnum.Training_Seq;
            Display = GmskPhaseAndFrequencyDisplayEnum.IQ_Error;
        }

        public override void Run()
        {
            base.Run();

            Vsa.GmskPhaseAndFrequency.TriggerSource = TriggerSource;
            Vsa.GmskPhaseAndFrequency.Averaging = Averaging.IsEnabled;
            Vsa.GmskPhaseAndFrequency.AverageCount = Averaging.Value;
            Vsa.GmskPhaseAndFrequency.AverageMode = AverageMode;
            Vsa.GmskPhaseAndFrequency.BurstSync = BurstSync;
            Vsa.GmskPhaseAndFrequency.Display = Display;

            Vsa.Apply(Measurements.GmskPhaseAndFrequency);

            string results = Vsa.Measure(Measurements.GmskPhaseAndFrequency);

            var errors = Vsa.QueryErrors();
            foreach (ScpiError err in errors)
                Log.Error(Vsa.Name + " Error: " + err.Message);

            UpgradeVerdict(Vsa.Verdict() ? Verdict.Pass : Verdict.Fail);

            List<string> resultNames = new List<string> { "RMS Phase Error (deg)", "Peak Phase Error (deg)", "Peak Phase Error Symbol Position", "Frequency Error (Hz)",
                                                          "IQ Origin Offset (dB)", "Trace Phase Sample (bits)", "Trace bit 0 Decision Offset", "Trace Sync Start (bits)",
                                                          "Trace Time Sample (sec)", "T0 Offset (sec)"};

            LogScpiValues(resultNames, results);
            PublishScpiValues("Gmsk Phase and Frequency", resultNames, results);
        }
    }
}
