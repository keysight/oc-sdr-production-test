﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer;
using System;
using System.Collections.Generic;
using static Keysight.Tap.ScpiInstrument;

namespace Keysight.Tap.Plugins.OpenCellular.TestSteps
{
    [Display("Tx Band Spur", Groups: new[] { "OpenCellular", "Measurements" }, Description: "This measurement checks that the transmitter does not transmit undesirable energy into the transmit band. This energy may cause interference for other users of the GSM system.")]
    public class TxBandSpur : TxBase
    {
        #region Settings
        [Display("Averaging", Group: "Meas Setup", Order: 70.0, Description: "Sets the number of bursts that are averaged. After the specified number of bursts (average counts), \nthe averaging mode (termination control) setting determines the averaging action.")]
        public Enabled<int> Averaging { get; set; }

        [Display("Average Mode", Group: "Meas Setup", Order: 70.2, Description: "Select the type of termination control used for the averaging function. This determines the averaging \naction after the specified number of data acquisitions (average count) is reached.")]
        public AverageModeEnum AverageMode { get; set; }

        [Display("Average Type", Group: "Meas Setup", Order: 70.3, Description: "Select the type of averaging. \nLog Pwr Avg – The log of the power is averaged. (This is also known as video averaging.) \nMaximum Hold – The maximum values are retained. \nPwr Avg – The power is averaged, providing the RMS of the voltage.")]
        public AverageTypeEnum AverageType { get; set; }

        [Display("Limit", Group: "Meas Setup", Order: 70.4, Description: "Sets the value for the test limit.")]
        public double Limit { get; set; }

        [Display("Limit Units", Group: "Meas Setup", Order: 70.4, Description: "dBm – Absolute limit \ndBc – Relative to Mean Transmit Power.")]
        public LimitTypeEnum LimitType { get; set; }

        [Display("Mode", Group: "Radio", Order: 20.0, Description: "Select measurement mode.")]
        public ModeEnum Mode { get; set; }
        #endregion

        public TxBandSpur()
        {
            Averaging = new Enabled<int>() { IsEnabled = true, Value = 30 };
            AverageMode = AverageModeEnum.Repeat;
            AverageType = AverageTypeEnum.Maximum_Hold;
            Limit = -36;
            LimitType = LimitTypeEnum.dBm;
            Mode = ModeEnum.GSM;
            Name = "{Mode} Tx Band Spur";
        }

        public override void Run()
        {
            ITxBandSpurMeasurement txBandSpur;
            Measurements measurement;

            base.Run();

            if (Mode == ModeEnum.GSM)
            {
                txBandSpur = Vsa.GmskTxBandSpur;
                measurement = Measurements.GmskTxBandSpur;
            }
            else if (Mode == ModeEnum.EDGE)
            {
                txBandSpur = Vsa.EdgeTxBandSpur;
                measurement = Measurements.EdgeTxBandSpur;
            }
            else
                throw new Exception("Unexpected Mode Type");

            txBandSpur.Averaging = Averaging.IsEnabled;
            txBandSpur.AverageCount = Averaging.Value;
            txBandSpur.AverageMode = AverageMode;
            txBandSpur.AverageType = AverageType;
            txBandSpur.Limit = Limit;
            txBandSpur.LimitType = LimitType;

            Vsa.Apply(measurement);

            string results = Vsa.Measure(measurement);

            var errors = Vsa.QueryErrors();

            foreach (ScpiError err in errors)
                Log.Error(Vsa.Name + " Error: " + err.Message);

            UpgradeVerdict(Vsa.Verdict() ? Verdict.Pass : Verdict.Fail);

            List<string> resultNames = new List<string> { "Worst Spur: Frequency from Channel Center (MHz)", "Worst Spur: Amplitude from Limit (dB)", "Worst Spur: Amplitude from Mean Tx Power (dB)" };

            LogScpiValues(resultNames, results);
            PublishScpiValues(Mode.ToString() + "Tx Band Spur", resultNames, results);
        }
    }
}
