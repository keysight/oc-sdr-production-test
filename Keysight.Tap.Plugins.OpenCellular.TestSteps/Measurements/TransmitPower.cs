﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
using System.Collections.Generic;
using Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer;
using static Keysight.Tap.ScpiInstrument;

namespace Keysight.Tap.Plugins.OpenCellular.TestSteps
{
    [Display("Transmit Power", Groups: new[] { "OpenCellular", "Measurements" }, Description: "The Burst Power (Transmit Power) measurement (at the base transceiver station) is used to determine the power delivered to the antenna system on the radio-frequency channel under test. The Burst Power measurement verifies the accuracy of the mean transmitted RF carrier power. This can be done across the frequency range and at each power step.")]
    public class TransmitPower : TxBase
    {
        #region Settings
        [Display("Averaging", Group: "Meas Setup", Order: 70.0, Description: "Sets the number of bursts that are averaged. After the specified number of bursts (average counts), \nthe averaging mode (termination control) setting determines the averaging action.")]
        public Enabled<int> Averaging { get; set; }

        [Display("Trigger Source", Group: "Trigger", Order: 10.1, Collapsed: true, Description: "Select trigger source: \nFree Run - Triggering occurs immediately after the sweep/measurement is initiated. \nVideo - Trigger condition is met when the video signal (the filtered and detected version of the input signal, including both RBW and VBW filtering) crosses the video trigger level. \nLine - A new sweep/measurement will start synchronized with the next cycle of the line voltage. \nExternal 1 - A new sweep/measurement will start when the external trigger condition is met using the external 1 input connector on the rear panel. \nExternal 2 - A new sweep/measurement will start when the external trigger condition is met using the external 2 input connector on the rear panel. \nRF Burst - A new sweep/measurement will start when an RF burst envelope signal is identified from the signal at the RF Input connector.")]
        public VsaTriggerEnum TriggerSource { get; set; }

        [Display("Limit", Group: "Limit", Order: 100, Collapsed: true, Description: "Select the pass/fail threshold level in dBm. Measured powers below this level will result in a failure verdict")]
        [Unit("dBm")]
        public Enabled<double> Limit { get; set; }
        #endregion

        public TransmitPower()
        {
            Averaging = new Enabled<int>() { IsEnabled = true, Value = 50 };
            TriggerSource = VsaTriggerEnum.RF_Burst;
            Limit = new Enabled<double>() { IsEnabled = false, Value = 0 };
        }

        public override void Run()
        {
            base.Run();

            Vsa.TxPower.Averaging = Averaging.IsEnabled;
            Vsa.TxPower.AverageCount = Averaging.Value;
            Vsa.TxPower.TriggerSource = TriggerSource;

            Vsa.Apply(Measurements.TXPower);

            string results = Vsa.Measure(Measurements.TXPower);
            double[] values = ParseCommaDelimitedStringToDoubles(results);

            var errors = Vsa.QueryErrors();
            foreach (ScpiError err in errors)
                Log.Error(Vsa.Name + " Error: " + err.Message);

            if (Limit.IsEnabled)
                UpgradeVerdict(values[1] >= Limit.Value ? Verdict.Pass : Verdict.Fail);

            List<string> resultNames = new List<string> { "Sample Time (sec)", "Power (dBm)", "Average Power (dBm)", "Number of Samples", "Threshold Value (dBm)", "Threshold Points",
                                                    "Maximum Value (dBm)", "Minimum Value (dBm)", "Full Burst Width", "Time Length (sec)", "Measured Points" };

            LogScpiValues(resultNames, results);
            PublishScpiValues("Transmit Power", resultNames, results);
        }
    }
}
