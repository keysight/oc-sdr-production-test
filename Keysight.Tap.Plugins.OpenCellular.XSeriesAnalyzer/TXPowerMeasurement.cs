﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
namespace Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer
{
    /// <summary>
    /// The Burst Power (Transmit Power) measurement (at the base transceiver station) is used to determine the power delivered to the antenna system on the radio-frequency channel under test. The Burst Power measurement verifies the accuracy of the mean transmitted RF carrier power. This can be done across the frequency range and at each power step.
    /// </summary>
    public class TxPowerMeasurement
    {
        private XSeriesAnalyzer instrument;

        /// <summary>
        /// Initializes a new instance of the <see cref="TxPowerMeasurement"/> class.
        /// </summary>
        /// <param name="instrument">The analysis instrument.</param>
        internal TxPowerMeasurement(XSeriesAnalyzer instrument)
        {
            this.instrument = instrument;
        }

        /// <summary>
        /// Gets or sets a value indicating whether averaging is enabled for this <see cref="TxPowerMeasurement"/>.
        /// </summary>
        /// <value>
        ///   <c>true</c> if averaging; otherwise, <c>false</c>.
        /// </value>
        public bool Averaging { get; set; }

        /// <summary>
        /// Gets or sets the average count.
        /// </summary>
        /// <value>
        /// The average count.
        /// </value>
        public int AverageCount { get; set; }

        /// <summary>
        /// Gets or sets the trigger source.
        /// </summary>
        /// <value>
        /// The trigger source.
        /// </value>
        public VsaTriggerEnum TriggerSource { get; set; }

        internal void Apply()
        {
            instrument.ScpiCommand(Scpi.Format(":TXP:AVER {0}", Averaging));
            instrument.ScpiCommand(":TXP:AVER:COUN {0}", AverageCount);
            instrument.ScpiCommand(Scpi.Format(":TRIG:TXP:SOUR {0}", TriggerSource));
        }
    }
}
