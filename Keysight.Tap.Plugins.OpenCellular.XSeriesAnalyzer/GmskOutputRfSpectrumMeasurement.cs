﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
namespace Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer
{
    /// <summary>
    /// The Output RF Spectrum measurement is the GSM version of the adjacent channel power (ACP) measurement.
    /// </summary>
    public class GmskOutputRfSpectrumMeasurement : IOutputRfSpectrumMeasurement
    {
        private XSeriesAnalyzer instrument;

        internal GmskOutputRfSpectrumMeasurement(XSeriesAnalyzer instrument)
        {
            this.instrument = instrument;
        }

        public bool Averaging { get; set; }

        public int AverageCount { get; set; }

        public VsaTriggerEnum TriggerSource { get; set; }

        public OutputRfSpectrumMeasEnum MeasurementType { get; set; }

        public OutputRfSpectrumMeasMethodEnum MeasMethod { get; set; }

        public OutputRfSpectrumOffsetFrequencyListEnum OffsetFrequencyList { get; set; }

        public bool WidebandNoise { get; set; }

        internal void Apply()
        {
            instrument.ScpiCommand(Scpi.Format(":ORFS:AVER {0}", Averaging));
            instrument.ScpiCommand(":ORFS:AVER:COUN {0}", AverageCount);
            instrument.ScpiCommand(Scpi.Format(":TRIG:ORFS:SOUR {0}", TriggerSource));
            instrument.ScpiCommand(Scpi.Format(":ORFS:TYPE {0}", MeasurementType));
            instrument.ScpiCommand(Scpi.Format(":ORFS:MEAS {0}", MeasMethod));
            instrument.ScpiCommand(Scpi.Format(":ORFS:LIST:SEL {0}", OffsetFrequencyList));
            instrument.ScpiCommand(Scpi.Format(":ORFS:WBN {0}", WidebandNoise));
        }
    }
}
