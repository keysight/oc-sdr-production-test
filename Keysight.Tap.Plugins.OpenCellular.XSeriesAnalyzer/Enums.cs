﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
namespace Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer
{ 
    /// <summary>
    /// Available XAppsAnalyzer Modes
    /// </summary>
    public enum Modes
    {
        /// <summary>
        /// The GSM/EDGE Mode
        /// </summary>
        GSM
    };

    /// <summary>
    /// Modes supported by interface-implementing measurements
    /// </summary>
    public enum ModeEnum
    {
        GSM,
        EDGE
    };

    /// <summary>
    /// Available Measurements
    /// </summary>
    public enum Measurements
    {
        /// <summary>
        /// Transmit Power Measurement
        /// </summary>
        [Scpi("TXP")]
        TXPower,
        [Scpi("ORFS")]
        GmskOutputRfSpectrum,
        [Scpi("EORF")]
        EdgeOutputRfSpectrum,
        [Scpi("PVT")]
        GmskPowerVsTime,
        [Scpi("EPVT")]
        EdgePowerVsTime,
        [Scpi("PFER")]
        GmskPhaseAndFrequency,
        [Scpi("EEVM")]
        EdgeEVM,
        /// <summary>
        /// GMSK Transmit Band Spurious Measurement
        /// </summary>
        [Scpi("TSP")]
        GmskTxBandSpur,
        /// <summary>
        /// EDGE Transmit Band Spurious Measurement
        /// </summary>
        [Scpi("ETSP")]
        EdgeTxBandSpur
    };

    /// <summary>
    /// Averaging Modes
    /// </summary>
    public enum AverageModeEnum
    {
        /// <summary>
        /// When Measure is set at Cont, data acquisitions continue indefinitely. After N averages, exponential averaging is used with a weighting factor of N (the displayed average count stops at N). Exponential averaging weights new data more than old data, which allows tracking of slow-changing signals. The weighting factor N is set using the Averages, Avg Bursts key.
        /// </summary>
        Exponential,
        /// <summary>
        /// When Measure is set at Cont, data acquisitions continue indefinitely. After N averages is reached, all previous result data is cleared and the average count is set back to 1. This is equivalent to being in Measure Single and pressing the Restart key when the Single measurement finishes.
        /// </summary>
        Repeat
    }

    /// <summary>
    /// Averaging Types
    /// </summary>
    public enum AverageTypeEnum
    {
        /// <summary>
        /// Simulates the traditional spectrum analyzer type of averaging by averaging the log of the power.
        /// </summary>
        [Scpi("LOG")]
        Log_Pwr_Avg,
        /// <summary>
        /// Keeps track of the maximum values.
        /// </summary>
        [Scpi("MAX")]
        Maximum_Hold,
        /// <summary>
        /// True power averaging that is equivalent to taking the RMS value of the voltage. It is the most accurate type of averaging.
        /// </summary>
        [Scpi("RMS")]
        Pwr_Avg
    }

    /// <summary>
    /// Trigger Sources/Types
    /// </summary>
    public enum VsaTriggerEnum
    {
        /// <summary>
        /// Free run triggering occurs immediately after the sweep/measurement is initiated.
        /// </summary>
        [Scpi("IMM")]
        Free_Run,
        /// <summary>
        /// The Video trigger condition is met when the video signal (the filtered and detected version of the input signal, including both RBW and VBW filtering) crosses the video trigger level.
        /// </summary>
        [Scpi("VID")]
        Video,
        /// <summary>
        /// A new sweep/measurement will start synchronized with the next cycle of the line voltage.
        /// </summary>
        [Scpi("LINE")]
        Line,
        /// <summary>
        /// A new sweep/measurement will start when the external trigger condition is met using the external 1 input connector on the rear panel.
        /// </summary>
        [Scpi("EXT1")]
        External_1,
        /// <summary>
        /// A new sweep/measurement will start when the external trigger condition is met using the external 2 input connector on the rear panel.
        /// </summary>
        [Scpi("EXT2")]
        External_2,
        /// <summary>
        /// A new sweep/measurement will start when an RF burst envelope signal is identified from the signal at the RF Input connector.
        /// </summary>
        [Scpi("RFB")]
        RF_Burst,
        /// <summary>
        /// Selects the internal periodic timer signal as the trigger
        /// </summary>
        [Scpi("PER")]
        Periodic_Timer
    };

    /// <summary>
    /// Limit Units
    /// </summary>
    public enum LimitTypeEnum
    {
        /// <summary>
        /// Absolute limit
        /// </summary>
        [Scpi("ABS")]
        dBm,
        /// <summary>
        /// Relative to Mean Transmit Power
        /// </summary>
        [Scpi("REL")]
        dBc
    }

    /// <summary>
    /// Available Bands
    /// </summary>
    public enum BandEnum
    {
        /// <summary>
        /// Primary GSM in the 900 MHz band
        /// </summary>
        [Scpi("PGSM")]
        P_GSM,
        /// <summary>
        /// Extended GSM in the 900 MHz band
        /// </summary>
        [Scpi("EGSM")]
        E_GSM,
        /// <summary>
        /// Railway GSM in the 900 MHz band
        /// </summary>
        [Scpi("RGSM")]
        R_GSM,
        /// <summary>
        /// DSC1800 band; also known as GSM–1800
        /// </summary>
        [Scpi("DCS1800")]
        DCS_1800,
        /// <summary>
        /// PCS1900 band; also known as GSM–1900
        /// </summary>
        [Scpi("PCS1900")]
        PCS_1900,
        /// <summary>
        /// GSM450 band
        /// </summary>
        [Scpi("GSM450")]
        GSM_450,
        /// <summary>
        /// GSM480 band
        /// </summary>
        [Scpi("GSM480")]
        GSM_480,
        /// <summary>
        /// GSM700 band
        /// </summary>
        [Scpi("GSM850")]
        GSM_850,
        /// <summary>
        /// GSM850 band, for IS–136HS
        /// </summary>
        [Scpi("GSM700")]
        GSM_700,
        /// <summary>
        /// T-GSM 810 band
        /// </summary>
        [Scpi("TGSM810")]
        T_GSM_810
    };

    /// <summary>
    /// Available Devices
    /// </summary>
    public enum RadioDeviceEnum
    {
        /// <summary>
        /// Base station transmitter test
        /// </summary>
        BTS,
        /// <summary>
        /// Mobile station transmitter test
        /// </summary>
        MS
    };

    /// <summary>
    /// Available Basestation Types
    /// </summary>
    public enum BtsEnum
    {
        /// <summary>
        /// Normal BTS.
        /// </summary>
        [Scpi("NORM")]
        Normal,
        /// <summary>
        /// Micro 1 BTS.
        /// </summary>
        [Scpi("MICR1")]
        Micro1,
        /// <summary>
        /// Micro 2 BTS.
        /// </summary>
        [Scpi("MICR2")]
        Micro2,
        /// <summary>
        /// Micro 3 BTS.
        /// </summary>
        [Scpi("MICR3")]
        Micro3,
        /// <summary>
        /// Pico 1 BTS.
        /// </summary>
        [Scpi("PICO1")]
        Pico1
    };

    /// <summary>
    /// Attenuator Modes for the Pre-Adjust for Min Clip Method
    /// </summary>
    public enum MinClipEnum
    {
        /// <summary>
        /// Turns Pre-Adjustment Off (default)
        /// </summary>
        Off,
        /// <summary>
        /// Selects only the electric attenuator to participate in auto ranging. This offers less wear on the mechanical attenuator and is usually faster.
        /// </summary>
        Electrical,
        /// <summary>
        /// In dual attenuator models, this selects both attenuators to participate in the auto-ranging.
        /// </summary>
        Combined
    };

    /// <summary>
    /// Automatic Alignment Methods
    /// </summary>
    public enum AutoAlignEnum
    {
        /// <summary>
        /// Auto Align, Normal turns on the automatic alignment of all measurement systems. The Auto Align, Normal selection maintains the instrument in warranted operation across varying temperature and over time.
        /// </summary>
        [Scpi("ON")]
        Normal,
        /// <summary>
        /// Auto Align, Partial disables the full automatic alignment and the maintenance of warranted operation for the benefit of improved measurement throughput. Accuracy is retained for the Resolution Bandwidth filters and the IF Passband, which is critical to FFT accuracy, demodulation, and many measurement applications. With Auto Align set to Partial, you are now responsible for maintaining warranted operation by updating the alignments when they expire. The Auto Align, Alert mechanism will notify you when alignments have expired. One solution to expired alignments is to perform the Align All, Now operation. Another is to return the Auto Align selection to Normal. Auto Align, Partial is recommended for measurements where the throughput is so important that a few percent of improvement is more valued than an increase in the accuracy errors of a few tenths of a decibel. One good application of Auto Align, Partial would be an automated environment where the alignments can be called during overhead time when the device-under-test is exchanged.
        /// </summary>
        [Scpi("PART")]
        Partial,
        /// <summary>
        /// Auto Align, Off disables automatic alignment and the maintenance of warranted operation, for the benefit of maximum measurement throughput. With Auto Align set to Off, you are now responsible for maintaining warranted operation by updating the alignments when they expire. The Auto Align, Alert mechanism will notify you when alignments have expired. One solution to expired alignments is to perform the Align All, Now operation. Another is to return the Auto Align selection to Normal. The Auto Align, Off setting is rarely the best choice, because Partial gives almost the same improvement in throughput while maintaining the warranted performance for a much longer time. The choice is intended for unusual circumstances such as the measurement of radar pulses where you might like the revisit time to be as consistent as possible.
        /// </summary>
        [Scpi("OFF")]
        Off
    }

    /// <summary>
    /// Immediate Alignment Types
    /// </summary>
    public enum AlignNowEnum
    {
        /// <summary>
        /// Immediately executes an alignment of all subsystems
        /// </summary>
        [Scpi("All")]
        All,
        /// <summary>
        /// Immediately executes an alignment of all subsystems except the RF subsystem
        /// </summary>
        [Scpi("NRF")]
        All_but_RF,
        /// <summary>
        /// Immediately executes an alignment of the RF subsystem
        /// </summary>
        [Scpi("RF")]
        RF
    }

    /// <summary>
    /// Available Sweep Modes
    /// </summary>
    public enum SweepModeEnum
    {
        /// <summary>
        /// Sets the analyzer for Single measurement operation.
        /// </summary>
        [Scpi("OFF")]
        Single,
        /// <summary>
        /// Sets the analyzer for Continuous measurement operation.
        /// </summary>
        [Scpi("ON")]
        Continuous
    }

    /// <summary>
    /// ORFS Measurement Types
    /// </summary>
    public enum OutputRfSpectrumMeasEnum
    {
        /// <summary>
        /// Performs both Modulation and Switching measurements.
        /// </summary>
        [Scpi("MSW")]
        Mod_and_Switch,
        /// <summary>
        /// Measures the spectrum due to the 3/8pi shift 8PSK modulation and noise.
        /// </summary>
        [Scpi("MOD")]
        Modulation,
        /// <summary>
        /// Measures the spectrum due to switching transients (burst ramping).
        /// </summary>
        [Scpi("SWIT")]
        Switching,
        /// <summary>
        /// Improves measurement speed by acquiring a full frame of data prior to performing the FFT calculation. This feature can only be used when all slots in the transmitted frame are active. 
        /// </summary>
        [Scpi("FFM")]
        Full_Frame_Mod
    };

    /// <summary>
    /// ORFS Measurement Methods
    /// </summary>
    public enum OutputRfSpectrumMeasMethodEnum
    {
        /// <summary>
        /// The measurement is done at all offsets in the offset frequency list.
        /// </summary>
        [Scpi("MULT")]
        Multi_Offset,
        /// <summary>
        /// The measurement is done in the frequency domain. For output RF spectrum due to modulation it is done using time-gated spectrum analysis to sweep the analyzer with the gate turned on for the desired portion of the burst only.
        /// </summary>
        [Scpi("SWEP")]
        Swept
    };

    /// <summary>
    /// Available ORFS Frequency Lists. See GSM/EDGE X-Apps documentation for details
    /// </summary>
    public enum OutputRfSpectrumOffsetFrequencyListEnum
    {
        /// <summary>
        /// Standard Frequency List
        /// </summary>
        [Scpi("STAN")]
        Standard,
        /// <summary>
        /// Short Frequency List
        /// </summary>
        [Scpi("SHOR")]
        Short,
        /// <summary>
        /// Custom Frequency List
        /// </summary>
        [Scpi("CUST")]
        Custom,
        /// <summary>
        /// Limited Custom Frequency List
        /// </summary>
        [Scpi("LCUS")]
        Limited_Custom
    };

    public enum PowerVsTimeBurstSyncEnum
    {
        [Scpi("TSEQ")]
        Training_Seq,
        [Scpi("RFB")]
        RF_Amplitude,
        [Scpi("NONE")]
        None
    }

    /// <summary>
    /// Available Instrument Display Options for Power Vs Time Measurement
    /// </summary>
    public enum PowerVsTimeDisplayEnum
    {
        /// <summary>
        /// Shows power vs. time and mask result for the burst
        /// </summary>
        [Scpi("ALL")]
        Burst,
        /// <summary>
        /// Shows Rising RF Envelope, Falling RF Envelope, and Numeric Results
        /// </summary>
        [Scpi("BOTH")]
        Rise_and_Fall,
        /// <summary>
        /// View optimized for Multi-Slot analysis
        /// </summary>
        [Scpi("MSL")]
        Multi_Slot
    }

    /// <summary>
    /// Available EDGE EVM Burst Sync Options
    /// </summary>
    public enum EdgeEVMBurstSyncEnum
    {
        /// <summary>
        /// The burst synchronization performs a demodulation of the burst and determines the start and stop of the useful part of the burst based on the midamble training sync sequence.
        /// </summary>
        [Scpi("TSEQ")]
        Training_Seq,
        /// <summary>
        /// The burst synchronization approximates the start and stop of the useful part of the burst without demodulation of the burst.
        /// </summary>
        [Scpi("RFB")]
        RF_Amplitude,
        /// <summary>
        /// The burst synchronization performs a demodulation of the burst and determines the start and stop of the useful part of the burst based on the midamble training sync sequence. (It's same as "Training Seq") The measurement start searching training sequence both on amplitude path and phase path to make synchronization
        /// </summary>
        [Scpi("PMOD")]
        Polar_Modulation,
        /// <summary>
        /// The measurement is performed without searching burst.
        /// </summary>
        [Scpi("NONE")]
        None
    }

    /// <summary>
    /// Available Instrument Display Options for EDGE EVM Measurement
    /// </summary>
    public enum EdgeEVMDisplayEnum
    {
        /// <summary>
        /// Displays Polar IQ Graph and Metrics
        /// </summary>
        [Scpi("POL")]
        IQ_Measured_Polar_Graph,
        /// <summary>
        /// Displays Magnitude Error, Phase Error, EVM, and Metrics
        /// </summary>
        [Scpi("ERR")]
        IQ_Error,
        /// <summary>
        /// Displays Metrics
        /// </summary>
        [Scpi("DBIT")]
        Data_Bits
    }

    /// <summary>
    /// Available GMSK Phase and Frequency Burst Sync Options
    /// </summary>
    public enum GmskPhaseAndFrequencyBurstSyncEnum
    {
        /// <summary>
        /// The burst synchronization performs a demodulation of the burst and determines the start and stop of the useful part of the burst based on the midamble training sync sequence.
        /// </summary>
        [Scpi("TSEQ")]
        Training_Seq,
        /// <summary>
        /// The burst synchronization approximates the start and stop of the useful part of the burst without demodulation of the burst.
        /// </summary>
        [Scpi("RFB")]
        RF_Amplitude
    }

    /// <summary>
    /// Available Instrument Display Options for GMSK Phase and Frequency Measurement
    /// </summary>
    public enum GmskPhaseAndFrequencyDisplayEnum
    {
        /// <summary>
        /// Displays Polar IQ Graph and Metrics
        /// </summary>
        [Scpi("POL")]
        IQ_Measured_Polar_Graph,
        /// <summary>
        /// Displays Magnitude Error, Phase Error, EVM, and Metrics
        /// </summary>
        [Scpi("ERR")]
        IQ_Error,
        /// <summary>
        /// Displays Metrics
        /// </summary>
        [Scpi("DBIT")]
        Data_Bits
    }

    /// <summary>
    /// Available ARFCN Presets
    /// </summary>
    public enum BmtFreqEnum
    {
        /// <summary>
        /// The highest ARFCN of the selected radio band
        /// </summary>
        Top,
        /// <summary>
        /// The middle ARFCN of the selected radio band
        /// </summary>
        Middle,
        /// <summary>
        /// The lowest ARFCN of the selected radio band
        /// </summary>
        Bottom
    }

    /// <summary>
    /// Available Burst Types
    /// </summary>
    public enum BurstTypeEnum
    {
        /// <summary>
        /// Sync (SCH) <para> Burst length = 142 symbols </para>
        /// </summary>
        [Scpi("SYNC")]
        Sync,
        /// <summary>
        /// Access (RACH) <para> Burst length = 88 symbols </para>
        /// </summary>
        [Scpi("ACC")]
        Access,
        /// <summary>
        /// Normal - NB (TCH & CCH) <para> Burst length = 142 symbols </para>
        /// </summary>
        [Scpi("NORM")]
        Normal,
        /// <summary>
        /// Higher Symbol Rate - HB (TCH & CCH) <para> Burst length = 169 symbols </para>
        /// </summary>
        [Scpi("HSR")]
        Higher_Symbol_Rate,
        /// <summary>
        /// Mixed (NB/HB for TSC sync) <para> Burst length = 142 symbols </para>
        /// </summary>
        [Scpi("MIX")]
        Mixed
    }

    /// <summary>
    /// Available Frequency Selection Options
    /// </summary>
    public enum FrequencySelectionEnum
    {
        /// <summary>
        /// Use the Center Frequency property to determine measurement frequency
        /// </summary>
        Center_Frequency,
        /// <summary>
        /// Use the BMT Frequency property to determine measurement frequency
        /// </summary>
        BMT_Frequency,
        /// <summary>
        /// Use the ARFCN to determine the measurement frequency
        /// </summary>
        ARFCN
    }
}
