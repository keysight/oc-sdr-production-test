﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



using System.Xml.Serialization;
using System;

namespace Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer
{
    /// <summary>
    /// Contains all GSM Mode-specific controls as well as enabled GSM Measurements
    /// </summary>
    /// <seealso cref="Keysight.Tap.ScpiInstrument" />
    [ShortName("XApps")]
    [Display("XApps Enabled Vector Signal Analyzer", Description: "Keysight Vector Signal Analyzer w/ N9071A Option Installed", Group: "OpenCellular")]
    public class XSeriesAnalyzer : ScpiInstrument
    {
        #region Settings
        /// <summary>
        /// The Burst Power (Transmit Power) measurement (at the base transceiver station) is used to determine the power delivered to the antenna system on the radio-frequency channel under test. The Burst Power measurement verifies the accuracy of the mean transmitted RF carrier power. This can be done across the frequency range and at each power step.
        /// </summary>
        [XmlIgnore]
        public TxPowerMeasurement TxPower { get; private set; }
        /// <summary>
        /// The Output RF Spectrum measurement is the GSM version of the adjacent channel power (ACP) measurement.
        /// </summary>
        [XmlIgnore]
        public GmskOutputRfSpectrumMeasurement GmskOutputRfSpectrum { get; private set; }
        /// <summary>
        /// The Output RF Spectrum measurement is the EDGE version of the adjacent channel power (ACP) measurement.
        /// </summary>
        [XmlIgnore]
        public EdgeOutputRfSpectrumMeasurement EdgeOutputRfSpectrum { get; private set; }
        /// <summary>
        /// Power vs. Time measures the mean transmit power during the “useful part” of GSM bursts and verifies that the power ramp fits within the defined mask. Power vs. Time also lets you view the rise, fall, and “useful part” of the GSM bursts. Using the “Multi-Slot” function, up to eight slots in a frame can be viewed at one time.
        /// </summary>
        [XmlIgnore]
        public GmskPowerVsTimeMeasurement GmskPowerVsTime { get; private set; }
        /// <summary>
        /// Phase and frequency error are the measures of modulation quality for GSM systems. Since GSM systems use relative phase to transmit information, the phase and frequency accuracy of the transmitter are critical to the systems performance and ultimately affect range.
        /// </summary>
        [XmlIgnore]
        public GmskPhaseAndFrequencyMeasurement GmskPhaseAndFrequency { get; private set; }
        /// <summary>
        /// Error Vector Magnitude measures the quality of the demodulated EDGE signal in the IQ plane
        /// </summary>
        [XmlIgnore]
        public EdgeEvmMeasurement EdgeEvm { get; private set; }
        /// <summary>
        /// Power vs. Time measures the mean transmit power during the “useful” part of bursts, and verifies that the power ramp fits within the defined mask. The “useful “ part of the burst is defined as, the 147 bits centered on the transition from bit 13 to bit 14 (the “TO” time point) of the 26 bit training sequence. The Power vs. Time measurement also lets you view the rise, fall, and the “useful” part of the bursts. Using the Multi-Slot function, up to eight slots in a frame can be viewed at one time.
        /// </summary>
        [XmlIgnore]
        public EdgePowerVsTimeMeasurement EdgePowerVsTime { get; private set; }
        /// <summary>
        /// This measurement checks that the transmitter does not transmit undesirable energy into the transmit band. This energy may cause interference for other users of the GSM system. 
        /// </summary>
        [XmlIgnore]
        public EdgeTxBandSpurMeasurement EdgeTxBandSpur { get; private set; }
        /// <summary>
        /// This measurement checks that the transmitter does not transmit undesirable energy into the transmit band. This energy may cause interference for other users of the EDGE system.
        /// </summary>
        [XmlIgnore]
        public GmskTxBandSpurMeasurement GmskTxBandSpur { get; private set; }

        /// <summary>
        /// Selects which property will be used to determine measurement frequency
        /// </summary>
        [XmlIgnore]
        public FrequencySelectionEnum FrequencySelection { get; set; }

        [XmlIgnore]
        public double Frequency { get; set; }

        [XmlIgnore]
        public bool AutoTrigger { get; set; }

        /// <summary>
        /// Gets or sets the Automatic Trigger time.
        /// </summary>
        /// <value>
        /// The automatic trigger time in milliseconds.
        /// </value>
        [XmlIgnore]
        public double AutoTriggerTime { get; set; }

        /// <summary>
        /// Gets or sets the GSM band.
        /// </summary>
        /// <value>
        /// The band.
        /// </value>
        [XmlIgnore]
        public BandEnum Band { get; set; }

        [XmlIgnore]
        public RadioDeviceEnum RadioDevice { get; set; }

        /// <summary>
        /// Gets or sets the BTS Type.
        /// </summary>
        /// <value>
        /// The BTS Type.
        /// </value>
        [XmlIgnore]
        public BtsEnum Bts { get; set; }

        /// <summary>
        /// Gets or sets the Mechanical Attenuation.
        /// </summary>
        /// <value>
        /// The Mechanical Attenuation in dB.
        /// </value>
        [XmlIgnore]
        public int MechAtten { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Electronic Attenuation is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if Electronic Attenuation is enabled; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool ElecAttenEnable { get; set; }

        /// <summary>
        /// Gets or sets the Electronic Attenuation.
        /// </summary>
        /// <value>
        /// The Electronic Attenuation in dB.
        /// </value>
        [XmlIgnore]
        public int ElecAtten { get; set; }

        /// <summary>
        /// Gets or sets the Minimum Clip Type.
        /// </summary>
        /// <value>
        /// The Minimum Clip Type.
        /// </value>
        [XmlIgnore]
        public MinClipEnum MinClip { get; set; }

        /// <summary>
        /// Gets or sets the Automatic Alignment mode.
        /// </summary>
        /// <value>
        /// The Automatic Alignment mode.
        /// </value>
        [XmlIgnore]
        public AutoAlignEnum AutoAlign { get; set; }

        [XmlIgnore]
        public SweepModeEnum SweepMode { get; set; }

        /// <summary>
        /// Gets or sets the External BTS Gain
        /// </summary>
        /// <value>
        /// The External BTS Gain
        /// </value>
        [XmlIgnore]
        public double ExtGainBts { get; set; }

        /// <summary>
        /// Gets or sets the External MS Gain
        /// </summary>
        /// <value>
        /// The External MS Gain
        /// </value>
        [XmlIgnore]
        public double ExtGainMs { get; set; }

        /// <summary>
        /// Gets or sets the Absolute RF Channel Number
        /// </summary>
        /// <value>
        /// The Absolute RF Channel Number
        /// </value>
        [XmlIgnore]
        public int Arfcn { get; set; }

        /// <summary>
        /// Gets or sets one of the available presets for ARFCN
        /// </summary>
        /// <value>
        /// The BMT Freq Preset
        /// </value>
        [XmlIgnore]
        public BmtFreqEnum BmtFreq { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Time Slot Searching is enabled.
        /// </summary>
        /// <value>
        ///  <c>true</c> if Time Slot Searching is enabled; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool TimeSlotEnabled { get; set; }

        /// <summary>
        /// Gets or sets the Time Slot to search for.
        /// </summary>
        /// <value>
        /// The Time Slot
        /// </value>
        [XmlIgnore]
        public int TimeSlot { get; set; }

        /// <summary>
        /// Gets or sets the Burst Type the analyzer will search for and to which it will synchronize
        /// </summary>
        /// <value>
        /// The Burst Type
        /// </value>
        [XmlIgnore]
        public BurstTypeEnum BurstType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Training Sequence Code selection is Automatic or Manual.
        /// </summary>
        /// <value>
        ///  <c>true</c> if Automatic Detection; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool TscEnabled { get; set; }

        /// <summary>
        /// Gets or sets the Training Sequence Code that determines which burst is to be measured
        /// </summary>
        /// <value>
        /// The Training Sequence Code
        /// </value>
        [XmlIgnore]
        public int Tsc { get; set; }
        #endregion

        public XSeriesAnalyzer()
        {
            IoTimeout = 5000;

            TxPower = new TxPowerMeasurement(this);
            GmskOutputRfSpectrum = new GmskOutputRfSpectrumMeasurement(this);
            EdgeOutputRfSpectrum = new EdgeOutputRfSpectrumMeasurement(this);
            GmskPhaseAndFrequency = new GmskPhaseAndFrequencyMeasurement(this);
            GmskPowerVsTime = new GmskPowerVsTimeMeasurement(this);
            EdgePowerVsTime = new EdgePowerVsTimeMeasurement(this);
            EdgeEvm = new EdgeEvmMeasurement(this);
            EdgeTxBandSpur = new EdgeTxBandSpurMeasurement(this);
            GmskTxBandSpur = new GmskTxBandSpurMeasurement(this);
        }

        /// <summary>
        /// Opens the connection to the Instrument.
        /// Assumes Visa Address property is specified.
        /// </summary>
        /// <exception cref="System.Exception"></exception>
        public override void Open()
        {
            base.Open();

            SetMode(Modes.GSM);
            WaitForOperationComplete();
            ModePreset();
        }

        #region Methods
        /// <summary>
        /// Switches instrument to the specified mode
        /// </summary>
        /// <param name="mode">The Mode.</param>
        public void SetMode(Modes mode)
        {
            ScpiCommand(":INST {0}", mode.ToString());
        }

        /// <summary>
        /// Initializes the specified measurement, waits for all queued commands to complete, then returns results of SCPI Fetch
        /// </summary>
        /// <param name="measurement">The Measurement.</param>
        /// <returns>Comma-delimited string of measurement results</returns>
        public string Measure(Measurements measurement)
        {
            System.Threading.Thread.Sleep(50); //TODO: Added to test Burst Not Found error
            ScpiCommand(Scpi.Format(":INIT:{0}", measurement));
            WaitForOperationComplete();

            return ScpiQuery(Scpi.Format(":FETC:{0}?", measurement));
        }

        /// <summary>
        /// Returns XApps result verdict
        /// </summary>
        /// <returns> <c>true</c> if verdict is Pass; otherwise, <c>false</c>. </returns>
        public bool Verdict()
        {
            string verdict = ScpiQuery(":CALC:CLIM:FAIL?");
            return (verdict == "0\n");
        }

        /// <summary>
        /// Optimizes input attenuation
        /// </summary>
        public void AdjustAttenMinClip()
        {
            ScpiCommand(":POW:RANG:OPT IMM");
        }

        /// <summary>
        /// Immediately initiates an alignment of the specified type
        /// </summary>
        /// <param name="alignNowType">Alignment Type.</param>
        public void AlignNow(AlignNowEnum alignNowType)
        {
            ScpiCommand(Scpi.Format(":CAL:{0}", alignNowType));
        }

        /// <summary>
        /// Returns the results of a SCPI Fetch. Used to gather further data following usage of Measure()
        /// </summary>
        /// <param name="measurement">The Measurement.</param>
        /// <param name="index">The Fetch index.</param>
        /// <returns>Comma-delimited string of measurement results</returns>
        public string Fetch(Measurements measurement, int index)
        {
            return ScpiQuery(Scpi.Format(":FETC:{0}{1}?", measurement, index));
        }

        /// <summary>
        /// Restores instrument to default configuration for the current mode. Equivalent to *RST SCPI command
        /// </summary>
        public void ModePreset()
        {
            ScpiCommand("*RST");
        }

        /// <summary>
        /// Completely resets all instrument variables to their default values. Most exhaustive reset option without power cycling
        /// </summary>
        public void ResetInstrument()
        {
            ScpiCommand(":SYST:DEF");
        }

        /// <summary>
        /// Restarts sweep. Equivalent to front-panel Restart key
        /// </summary>
        public void Restart()
        {
            ScpiCommand(":INIT:REST");
        }

        public void Apply(Measurements measurement)
        {
            ScpiCommand(Scpi.Format(":TRIG:ATR:STAT {0}", AutoTrigger));
            ScpiCommand(":TRIG:ATR {0} ms", AutoTriggerTime);
            ScpiCommand(Scpi.Format(":RAD:STAN:BAND {0}", Band));
            ScpiCommand(":RAD:DEV {0}", RadioDevice);
            ScpiCommand(Scpi.Format(":RAD:DEV:BASE {0}", Bts));
            ScpiCommand(":POW:ATT {0}", MechAtten);
            ScpiCommand(Scpi.Format(":POW:EATT:STAT {0}", ElecAttenEnable));
            ScpiCommand(":POW:EATT {0}", ElecAtten);
            ScpiCommand(":POW:RANG:OPT:ATT {0}", MinClip);
            ScpiCommand(Scpi.Format(":CAL:AUTO {0}", AutoAlign));
            ScpiCommand(Scpi.Format(":INIT:CONT {0}", SweepMode));
            ScpiCommand(":CORR:BTS:GAIN {0}", ExtGainBts);
            ScpiCommand(":CORR:MS:GAIN {0}", ExtGainMs);

            ScpiCommand(Scpi.Format(":CHAN:SLOT:AUTO {0}", TimeSlotEnabled));
            ScpiCommand(":CHAN:SLOT {0}", TimeSlot);
            ScpiCommand(Scpi.Format(":CHAN:BURS {0}", BurstType));
            ScpiCommand(Scpi.Format(":CHAN:TSC:AUTO {0}", TscEnabled));
            ScpiCommand(":CHAN:TSC {0}", Tsc);

            switch (FrequencySelection)
            {
                case FrequencySelectionEnum.Center_Frequency:
                    ScpiCommand(":FREQ:CENT {0}", Frequency);
                    break;
                case FrequencySelectionEnum.BMT_Frequency:
                    ScpiCommand(":CHAN:ARFC:{0}", BmtFreq);
                    break;
                case FrequencySelectionEnum.ARFCN:
                    ScpiCommand(":CHAN:ARFC {0}", Arfcn);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            switch (measurement)
            {
                case Measurements.TXPower:
                    TxPower.Apply();
                    break;
                case Measurements.GmskOutputRfSpectrum:
                    GmskOutputRfSpectrum.Apply();
                    break;
                case Measurements.EdgeOutputRfSpectrum:
                    EdgeOutputRfSpectrum.Apply();
                    break;
                case Measurements.GmskPowerVsTime:
                    GmskPowerVsTime.Apply();
                    break;
                case Measurements.EdgePowerVsTime:
                    EdgePowerVsTime.Apply();
                    break;
                case Measurements.GmskPhaseAndFrequency:
                    GmskPhaseAndFrequency.Apply();
                    break;
                case Measurements.EdgeEVM:
                    EdgeEvm.Apply();
                    break;
                case Measurements.GmskTxBandSpur:
                    GmskTxBandSpur.Apply();
                    break;
                case Measurements.EdgeTxBandSpur:
                    EdgeTxBandSpur.Apply();
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(measurement), measurement, null);
            }
        }
        #endregion
    }
}
