﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
namespace Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer
{
    /// <summary>
    /// Power vs. Time measures the mean transmit power during the “useful part” of GSM bursts and verifies that the power ramp fits within the defined mask. Power vs. Time also lets you view the rise, fall, and “useful part” of the GSM bursts. Using the “Multi-Slot” function, up to eight slots in a frame can be viewed at one time.
    /// </summary>
    public class GmskPowerVsTimeMeasurement : IPowerVsTimeMeasurement
    {
        private XSeriesAnalyzer instrument;

        internal GmskPowerVsTimeMeasurement(XSeriesAnalyzer instrument)
        {
            this.instrument = instrument;
        }

        public bool Averaging { get; set; }

        public int AverageCount { get; set; }

        public VsaTriggerEnum TriggerSource { get; set; }

        public AverageModeEnum AverageMode { get; set; }

        public int MeasureTime { get; set; }

        public PowerVsTimeBurstSyncEnum BurstSync { get; set; }

        public PowerVsTimeDisplayEnum Display { get; set; }

        internal void Apply()
        {
            instrument.ScpiCommand(":CONF:PVT");
            instrument.ScpiCommand(Scpi.Format(":PVT:AVER {0}", Averaging));
            instrument.ScpiCommand(":PVT:AVER:COUN {0}", AverageCount);
            instrument.ScpiCommand(Scpi.Format(":TRIG:PVT:SOUR {0}", TriggerSource));
            instrument.ScpiCommand(":PVT:AVER:TCON {0}", AverageMode);
            instrument.ScpiCommand(":PVT:SWE:TIME {0}", MeasureTime);
            instrument.ScpiCommand(Scpi.Format(":PVT:BSYN:SOUR {0}", BurstSync));
            instrument.ScpiCommand(Scpi.Format(":DISP:PVT:VIEW {0}", Display));
        }
    }
}
