﻿// Copyright 2017 KEYSIGHT TECHNOLOGIES
// Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
namespace Keysight.Tap.Plugins.OpenCellular.XSeriesAnalyzer
{
    /// <summary>
    /// This measurement checks that the transmitter does not transmit undesirable energy into the transmit band. This energy may cause interference for other users of the GSM system. 
    /// </summary>
    public class EdgeTxBandSpurMeasurement : ITxBandSpurMeasurement
    {
        private XSeriesAnalyzer instrument;

        internal EdgeTxBandSpurMeasurement(XSeriesAnalyzer instrument)
        {
            this.instrument = instrument;
        }

        public int AverageCount { get; set; }

        public AverageModeEnum AverageMode { get; set; }

        public AverageTypeEnum AverageType { get; set; }

        public bool Averaging { get; set; }

        public double Limit { get; set; }

        /// <summary>
        /// Gets or sets the units of the limit.
        /// </summary>
        public LimitTypeEnum LimitType { get; set; }

        internal void Apply()
        {
            instrument.ScpiCommand(":ETSP:AVER:COUN {0}", AverageCount);
            instrument.ScpiCommand(":ETSP:AVER:TCON {0}", AverageMode);
            instrument.ScpiCommand(Scpi.Format(":ETSP:AVER:TYPE {0}", AverageType));
            instrument.ScpiCommand(Scpi.Format(":ETSP:AVER {0}", Averaging));
            instrument.ScpiCommand(":CALC:ETSP:LIM {0}", Limit);
            instrument.ScpiCommand(Scpi.Format(":CALC:ETSP:LIM:TEST {0}", LimitType));
        }
    }
}
